Assignment to configure a blockchain network by using heterogeneous clients
====

### Description

The assignment is to configure a blockchain network with [Geth](https://geth.ethereum.org/) and [Parity](https://www.parity.io/ethereum/) clients run in the Docker environment.

### Technical details

Below are the details describing what it is necessary to achieve assignment and what we will pay attention on when review your submission.

1. By using `docker-compose` you need to automate setting up of two nodes. The nodes must be up and running on a standalone testing system.
2. These nodes must be in the same network and synchronized. So, the blocks produced by one of the node should appear on the second node. You could configure the network as so only one node will produce new blocks.
3. The network must use the proof-of-work consensus.
4. The chain must have at least one pre-mined account `0xBF3d6f830CE263CAE987193982192Cd990442B53` with `100` ether on the balance.
5. Docker container of every node must expose a port for RPC connection: 8545 (geth) and 8546 (parity).
6. In order to prove that nodes are being synchronized you need to provide a JS script for the geth node to send a transaction and explicit instructions how to use this script.

Based on this your solution (a GitLab repo as it is described below) must contain at least:

1. `docker-compose.yml`
2. The file `HOWTO.md` with detailed step-by-step instruction how to check you solution. Remember, you solution must not require anything special except `Docker` and `docker-compose` on the testing system. If you would like to extend this list please contact to [Alexander Kolotov](https://t.me/AlexanderKolotov) directly.
3. Any additional script/files/whatever that are required by your setup to demonstrate completion of the assignment.
