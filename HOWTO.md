## Tutorial: How to run two Etherium clients and connect them by the net (Geth and Parity clients)

It is supposed that docker and docker-compose are installed succesfullly. Ensure it by "docker --version" and "docker-compose --version" commands.

After it you need to go through certain steps which are described below by executing these commands in command line (in the root directory of the git):

1. First of all get latest parity client `sudo docker pull parity/parity`
2. Next, do the same for Geth client `sudo docker pull ethereum/client-go`
3. Now build your copy of net `sudo docker-compose build`
4. Finally, run it `sudo docker-compose up`

## Notice: You may don't use the "sudo" with all docker commands if you go through all the steps of post-installation guide: https://docs.docker.com/install/linux/linux-postinstall/


Now both the nodes nodes (geth_1 and parity_1) are up and running.
## Notice: If you want to stop containers just stop them, not kill, because if you will kill them you will need to rebuild the docker-compose!!!
## Notice: The chain must have at least one pre-mined account 0xBF3d6f830CE263CAE987193982192Cd990442B53 with 100 ether on the balance. Actually, this account is pre-mined but has more ether than 100.

In order to prove that nodes are being synchronized the script with sending the transaction is provided and can be called by next commands:

First call two next scripts to print the amount of coins on both accounts:
`sudo docker exec -it testnetfixed_geth_1 geth attach http://localhost:8545 --jspath "./" --exec 'loadScript("chech_account_with_money.js")'`
`sudo docker exec -it testnetfixed_geth_1 geth attach http://localhost:8545 --jspath "./" --exec 'loadScript("chech_account_without_money.js")'`

Next do teh transaction by the next script: 
 `sudo docker exec -it testnetfixed_geth_1 geth attach http://localhost:8545 --jspath "./" --exec 'loadScript("transact_geth.js")'`

Finally, repeat the first two scripts to ensure that the balances succesfully changed. Due to generating the DAG (approximately no more than 5 minutes) the transaction will not be executed. Wait this time and then check the balances again.

